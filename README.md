# OAK deployments

This project containes configurations for different deployments of the system components of Oak.

# dev
![local dev](/img/dev.png "dev configuration")

A simple deployment configuration for local development. It makes an Apache Jena Fuseki SPARQL server available on localhost:3032, backed by a Delta server storing data in the lokal ./store directory.


# devHA
A deployment configuration creating a HA SPARQL storage backend. It makes two Apache Jena Fuseki SPARQL servers backed by three Delta servers available on localhost:3032 through an nginx reverse proxy.


# test
![local test](/img/test.png "test configuration")

A deployment configuration for spinning up a local test environment of CSS with the sparql-redis-endpoint.json configuration. It makes CSS available on localhost:3000 and the backing Apache Jena Fuseki SPARQL server available on localhost:3032. The Delta patch storage server stores data in the lokal ./store directory, and there is also a Redis store being spinned up that's being used by CSS for key-value storage by this configuration.


# test-ssl
![cloud test](/img/test-ssl.png "test configuration for cloud deployment with ssl")

A deployment configuration for the cloud test environment of CSS with the sparql-redis-endpoint-no-idp.json configuration. The CSS is fronted with an nginx with ssl configuration, and a letsencrypt certbot that fetches valid ssl certificates. The configuration uses the registered DNS name pods.dev.oak.sunet.se


# testHA
![local testHA](/img/testHA.png "test HA CSS configuration")

A deployment configuration for testing CSS HA based on the sparql-redis-endpoint.json configuration. It makes two CSS servers available on localhost:3000 through an nginx reverse proxy. The two CSS servers are using a single Fuseki SPARQL server for RDF storage and a singe Redis server for key-value storage.


# Run selected configuration in Docker


```
cd dev[HA]
docker compose up
```

Tear down with ```Ctrl-C``` or ```docker compose down``` from another terminal

# Check RDF content of the Fusei server

```
curl --data 'query=SELECT ?g ?s ?p ?o WHERE { GRAPH ?g { ?s ?p ?o } . }' --data 'output=text' http://localhost:3032/ds
```

# Using local images for building the containers
The docker-compose files are pointing to GitLab repositories for their build context. Tis can be overriden by creating a .env file in the respective folder where the docker-compose file resides and add a definition for overriding the build context location. For example:
```
CSS_REPO=../../solid/community-server
````
This will make the local development copy of CSS to be used when building the image.

# Forcing image rebuild
To force a rebuild of an image, remove the image from Docker. For example to remove an image named css:
```
docker rmi css
````
If the image is still used by any running or stopped containers, the running containers have to be stopped and removed before the image can be removed.

# Cloud deployments - oak.sunet.se
The configurations can be deployed in the cloud environment by cloud-init templates

The css-test/cloud-init.yml file contains deployment descriptions to spin up a single instance Community Solid Server with Redis and Fuseki storages on a single VM.
